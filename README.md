## FrameProtector [![Poggit-CI](https://poggit.pmmp.io/ci.badge/BrianZP/FrameProtector/FrameProtector/master)](https://poggit.pmmp.io/ci/BrianZP/FrameProtector)

	 FrameProtector plugin for PocketMine-MP
     Copyright (C) 2014 BrianZP <https://github.com/BrianZP/FrameProtector>

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU Lesser General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

#### Allows you to put protection on Item Frames!!

## Features:

- Enables protection for ItemFrames,
- Players can no longer grief the items inside ItemFrames,
- Perfect for making shops with ItemFrames,

## Dependancy:

This plugin depends on **[iProtector](https://github.com/LDX-MCPE/iProtector)** for the protection of ItemFrames.

## Usage:

- Put the auto build [.phar](https://poggit.pmmp.io/ci/BrianZP/FrameProtector) file in your servers plugin folder,
- Protect the area with ItemFrames using iProtector,
- Be sure to flag the area with Edit and Touch flags set to true.

### Commands:

No commands and permissions at all.

### Supported API's

- 3.0.0-ALPHA5
- 3.0.0-ALPHA6

## NOTE

This plugin is designed for and tested on **[PocketMine-MP](https://github.com/pmmp/PocketMine-MP)** only and we do not support any other variations, forks or spoons.
Compatibility with unofficial variants can occasionally be found, but **do not expect support if you are using anything other than PocketMine-MP**.

