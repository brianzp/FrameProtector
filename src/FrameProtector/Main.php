<?php

/*
 * FrameProtector plugin for PocketMine-MP
 * Copyright (C) 2014 BrianZP <https://github.com/BrianZP/FrameProtector>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
*/

namespace FrameProtector;

use pocketmine\plugin\PluginBase;
use pocketmine\Server;
use pocketmine\utils\TextFormat as TF;

class Main extends PluginBase{

    public function onLoad(){
                $this->getLogger()->info(TF::AQUA."Plugin is Loading. Be patient...");
				$this->iProtector = $this->getServer()->getPluginManager()->getPlugin("iProtector");
    }
    public function onEnable(){
        $this->getServer()->getPluginManager()->registerEvents(new Events($this), $this);
                $this->getLogger()->info(TF::GREEN."Plugin Enabled.");
    }
    public function onDisable(){
                $this->getLogger()->info(TF::RED."Plugin Disabled.");
    }
}
